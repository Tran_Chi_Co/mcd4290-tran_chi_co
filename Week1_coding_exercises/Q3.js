/*
Given the following information about a person:
First Name = Kanye
Last Name = West
Birth Date = 8 June 1977
Annual Income = 150000000.00
Write some code to create an Object with the information above as properties with the appropriate
data type for each property. Once you have created this object, print out the details to the console.
Example output:
Kanye West was born on 8 June 1977 and has an annual income of $15000000
*/
let human={ // this syntax is to make an object
  First_name:" Kanye ",
  Last_name:" West ",
  Birth_Date:" 8 June 1977 ",
  Annual_income: 150000000
}
console.log(human.First_name+human.Last_name+"was born on"+human.Birth_Date+"and has an annual income of $"+human.Annual_income)