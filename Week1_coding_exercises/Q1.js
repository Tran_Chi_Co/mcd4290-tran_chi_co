/*
Given the following circle with a radius of r = 4, write code to find the circumference of this circle c
and print the result to console to 2 decimal places.
*/
let R= 4;
let a=Math.PI; //Math.PI is the command for pi value
let p= 2*a*R;
console.log(p.toFixed(2))
// toFixed will turn value of number into a string then cut it to the required decimal