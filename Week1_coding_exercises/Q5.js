/*
In the following code what is wrong with the Boolean expression?
• Try plugging in different values of year and check the output.
• Fix the problem?
• Make sure you understand the operator precedence order especially with respect to
assignment, relationals and logicals.
let year;
let yearNot2015Or2016;
year = 2000;
yearNot2015Or2016 = year !== 2015 || year !== 2016;
console.log(yearNot2015Or2016);
*/

/* the given code is wrong because when we set the year to 2015
 the first condition, year!==2015 is false
but the second condition is true
when using OR ||, the result will be true but the year is 2015 cannot be true
*/
// fixed code
let year;
let yearNot2015Or2016;
year = 2015;
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016);
