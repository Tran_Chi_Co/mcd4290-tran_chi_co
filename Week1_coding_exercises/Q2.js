/*
Using only the two literal Strings:
let animalString = "cameldogcatlizard";
let andString = " and ";
Use both String methods substr(…) and substring(…) to output “cat and dog”.
Look up documentation (w3schools or Mozilla) to understand the different semantics of the
parameters of these 2 methods. Be prepared to show your demonstrator the documentation you
used and to explain the difference between substr(…) and substring(…). 
*/
let animalString = "cameldogcatlizard";
let andString = " and ";
//method1
//substr(a,b) from possition a, with b elements (b is a number)
a= animalString.substr(5,3);
let b= animalString.substr(8,3);
console.log(a+andString+b)
//method 2
//substring(a.b) a is the possition of first element to the possition before b
a= animalString.substring(5,8);
b= animalString.substring(8,11);
console.log(a+andString+b)